#include "Entry_record.h"

// this function formats time from time_t to string that can be sent to Mariadb as datetime
void format_datetime (time_t arg, std::string& res) {
    struct tm *cur_datetime = localtime(&arg);
    std::stringstream ss;
    ss << std::to_string(cur_datetime->tm_year + 1900);
    ss << "-";
    ss << std::setw(2) << std::setfill('0') << std::to_string(cur_datetime->tm_mon + 1);
    ss << "-";
    ss << std::setw(2) << std::setfill('0') << std::to_string(cur_datetime->tm_mday);
    ss << " ";
    ss << std::setw(2) << std::setfill('0') << std::to_string(cur_datetime->tm_hour);
    ss << ":";
    ss << std::setw(2) << std::setfill('0') << std::to_string(cur_datetime->tm_min);
    ss << ":";
    ss << std::setw(2) << std::setfill('0') << std::to_string(cur_datetime->tm_sec);
    res = ss.str();
}

// this function registers an entry and sends its time to database
void Entry_record::register_entry (const HttpRequestPtr& req, 
                                   std::function<void (const HttpResponsePtr &)> &&callback) 
{
    time_t entry_time;
    time(&entry_time);
    
    std::string entry_time_str;
    format_datetime(entry_time, entry_time_str);
    std::cout << "New entry: " << entry_time_str << std::endl;
    
    auto db_pointer = drogon::app().getDbClient("entry_database");
    if (db_pointer == nullptr) {
        std::cout << "Database pointer in register_entry is null" << std::endl;
    }
    db_pointer->execSqlAsync("INSERT entries VALUE ('" + entry_time_str + "');",
                             [](const drogon::orm::Result &r) {},
                             [](const drogon::orm::DrogonDbException &e) {
                                 std::cerr << "error while writing an entry:" << e.base().what() << std::endl;
                             });
    
    auto resp=HttpResponse::newHttpResponse();
    resp->setStatusCode(k200OK);
    resp->setContentTypeCode(CT_TEXT_HTML);
    resp->setBody("Your entry: " + entry_time_str + "\n");
    callback(resp);
}

// this function gets every entry from the database and sends it back in json format
void Entry_record::get_entries(const HttpRequestPtr& req, 
                               std::function<void (const HttpResponsePtr &)> &&callback)
{
    Json::Value ret;
    
    auto db_pointer = drogon::app().getDbClient("entry_database");
    if (db_pointer == nullptr) {
        std::cout << "Database pointer in get_entries is null" << std::endl;
    }
    auto fut = db_pointer->execSqlAsyncFuture("SELECT * FROM entries;");
    try {
        auto result = fut.get();
        Json::Value entry_arr(Json::arrayValue);
        for (auto entry : result) {
            entry_arr.append(entry["entry_date_time"].as<std::string>());
        }
        ret["entries"] = entry_arr;
    } catch (const drogon::orm::DrogonDbException &e) {
        std::cerr << "error while getting entry table:" << e.base().what() << std::endl;
    }
    
    std::cout << "Entries information requested" << std::endl;
    auto resp = HttpResponse::newHttpJsonResponse(ret);
    callback(resp);
}
