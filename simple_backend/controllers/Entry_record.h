#pragma once

#include <drogon/HttpController.h>
#include <ctime>
#include <string>
#include <sstream>
#include <iomanip>

using namespace drogon;

class Entry_record : public drogon::HttpController<Entry_record>
{
  public:
    METHOD_LIST_BEGIN
    
    // adding two methods described in task
    ADD_METHOD_TO(Entry_record::register_entry, "/visit_website", Get);
    ADD_METHOD_TO(Entry_record::get_entries, "/get_website_visits", Get);
    
    METHOD_LIST_END
    
    // adding function prototypes
    void register_entry(const HttpRequestPtr& req, 
                        std::function<void (const HttpResponsePtr &)> &&callback);
                        
    void get_entries(const HttpRequestPtr& req, 
                     std::function<void (const HttpResponsePtr &)> &&callback);
};
