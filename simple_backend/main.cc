#include <drogon/drogon.h>

int main() {
    // loading config file
    drogon::app().loadConfigFile("../config.json");
    // running server
    drogon::app().run();
    return 0;
}
