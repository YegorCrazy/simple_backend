-- creating database if it doesn't exist --
CREATE DATABASE IF NOT EXISTS simple_backend_db;

USE simple_backend_db;

-- creating entry table if it doesn't exist --
CREATE TABLE IF NOT EXISTS entries (entry_date_time DATETIME);

-- creating user to login from server only --
-- address to login from is 'service_name.network_name'. If you use custom network, unfortunately, you have to change this script and rebuild image --
CREATE USER IF NOT EXISTS 'db_user'@'simple_backend_server_1.simple_backend_default' IDENTIFIED BY 'some_pwd';

GRANT ALL PRIVILEGES ON simple_backend_db.* TO 'db_user'@'simple_backend_server_1.simple_backend_default' IDENTIFIED BY 'some_pwd';
