# Simple backend server

This is a simple backend-server that stores time of user entry.

## Requirements

You need to have Docker and Docker Compose to use this server.

## Installation

1) Clone this repository;
2) Edit configuration file (.env) as described below;
3) Run command
```bash
docker-compose up
```
if you want to run the server in terminal tab or
```bash
docker-compose up -d
```
if you want it to work in detached mode.

## Configuration

Options from .env file are the following:

- DATABASE_STORAGE: a folder where database information will be stored between server starts.
- DATABASE_ROOT_PASSWORD: a password for root user of Mariadb server.
- SERVER_PORT: a port number for using server itself.
- DATABASE_PORT: a port number for Mariadb server that stores server information.
- TIMEZONE: timezone that you are currently in (Continent/City format).

Please do not use spaces when configuring .env file. All this options must be initialized before starting server.

**PLEASE DO NOT CHANGE COMPOSE_PROJECT_NAME OPTION OR SERVER WON'T WORK.**

## Usage

There are two get-requests that server can handle.

- visit_website: server saves current date and time to database table and sends back some simple html answer;
- get_website_visits: server sends back a JSON answer with an array named "entries" that contains all the stored entries. Every entry is stored as a string "YYYY-MM-DD hh:mm:ss".

Entries are stored in DATABASE_STORAGE folder when server isn't working.

## Contributing
For any help please contact me!
- Email: egor2002288@gmail.com
- Telegram: @time_is_ticking